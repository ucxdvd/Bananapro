#extip=163.172.201.212
extif=wlan0

#intip=10.0.0.1
intif=eth0

dmzhost=192.168.2.253
# activate masquerading
iptables -t nat -A POSTROUTING -o $extif -j MASQUERADE
# enabling ip forwarding
sysctl -w net.ipv4.ip_forward=1
#alternative, oldskool way
echo 1 > /proc/sys/net/ipv4/ip_forward

# set up portforwarding

sport=80
target=$dmzhost
dport=80
iptables -t nat -A PREROUTING -p tcp -i $intif --dport $sport -j DNAT --to-destination $target:$dport -m comment --comment "Port forward $sport -> $dport"
iptables -A FORWARD -p tcp -d $extip --dport $sport -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT

sport=443
target=$dmzhost
dport=443
iptables -t nat -A PREROUTING -p tcp -i $extif -dport $sport -j DNAT --to-destination $target:$dport -m comment --comment "Port forward -> 443"
iptables -A FORWARD -p tcp -d $extip --dport $port -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT

sport=32400
target=$dmzhost
dport=32400
iptables -t nat -A PREROUTING -p tcp -i $extif --dport $sport -j DNAT --to-destination $target:$dport -m comment --comment "Port forward $sport -> $dport"
iptables -A FORWARD -p tcp -d $extip --dport $sport -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT

# show rules
iptables -L
iptables --table nat --list
