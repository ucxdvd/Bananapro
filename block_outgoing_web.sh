set -x
# put iptables stuff here
#iptables -A OUTPUT -p tcp -d 127.0.1.1 --match multiport --dports 80,443 -j ACCEPT
#iptables -A OUTPUT -p tcp -d edge-oculus-shv-01-bru2.fbcdn.net --match multiport --dports 80,443 -j REJECT
iptables -I FORWARD -p tcp  --match multiport --dports 80,443 -j REJECT
#iptables -A OUTPUT -p tcp --match multiport --dports 25,587,465 -j REJECT
# end of iptables stuff
# watchdog stuff
#echo test your config now, and interrupt this script
#echo if not, it will reboot this system within 30 seconds
#sleep 30
#reboot
/root/ip_forward.sh
