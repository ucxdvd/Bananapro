echo 1 > /proc/sys/net/ipv4/ip_forward
wan=wlan0
lan=eth0
iptables -t nat -A POSTROUTING -o $wan -j MASQUERADE
iptables -A FORWARD -i $lan -o $lan -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i $wan -o $wan -j ACCEPT
