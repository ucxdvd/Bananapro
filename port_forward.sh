dmzhost=192.168.2.253
dport=32400
iptables -A FORWARD -m state -p tcp -d $dmzhost --dport $dport --state NEW,ESTABLISHED,RELATED -j ACCEPT
iptables -t nat -A PREROUTING -p tcp --dport $dport -j DNAT --to-destination $dmzhost:$dport
