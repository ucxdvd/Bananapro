set -x
export PROXYSERVER=192.168.2.11
export PROXYPORT=8118
export localnetwork=192.168.2.0/24
iptables -t nat -A PREROUTING -i eth0  -p tcp --dport 80 -j DNAT --to $PROXYSERVER:$PROXYPORT
iptables -t nat -A PREROUTING -i eth0  -p tcp --dport 443 -j DNAT --to $PROXYSERVER:$PROXYPORT
iptables -t nat -A POSTROUTING -o eth0 -s $localnetwork -d $PROXYSERVER -j SNAT --to 192.168.2.11
iptables -A FORWARD -s $localnetwork -d $PROXYSERVER -i eth0 -o eth0 -p tcp --dport $PROXYPORT -j ACCEPT
