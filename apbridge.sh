hostapd -B /etc/hostapd/hostapd.conf
#ifconfig wlan0 172.16.0.1
echo 1 > /proc/sys/net/ipv4/ip_forward
#iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
#iptables -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
#iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT
brctl addbr br0
brctl addif br0 eth0
brctl addif br0 wlan0
ifconfig eth0 0.0.0.0
ifconfig eth0 up
route del default
ifconfig wlan0 0.0.0.0
ifconfig wlan0 up
ifconfig br0 192.168.0.11
ifconfig br0 up
route add default gw 192.168.0.1
