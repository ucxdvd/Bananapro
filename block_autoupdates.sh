set -x
# put iptables stuff here

## clear forward chain
iptables -F FORWARD
## enable ip masquerating & ip forwarding
echo 1 > /proc/sys/net/ipv4/ip_forward
wan=wlan0
lan=eth0
iptables -t nat -A POSTROUTING -o $wan -j MASQUERADE
iptables -A FORWARD -i $lan -o $lan -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i $wan -o $wan -j ACCEPT

### explicitly allow traffic
## allow dns
iptables -A FORWARD -p tcp --dport 53 -j ACCEPT # DNS via tcp
iptables -A FORWARD -p udp --dport 53 -j ACCEPT # DNS via udp

## allow dns
iptables -A FORWARD -p tcp --dport 123 -j ACCEPT # DNS via tcp
iptables -A FORWARD -p udp --dport 123 -j ACCEPT # DNS via udp

## allow openvpn
iptables -A FORWARD -p tcp --dport 1194 -j ACCEPT # openvpn via tcp
iptables -A FORWARD -p udp --dport 1194 -j ACCEPT # openvpn via udp

## allow ssh out
iptables -A FORWARD -p tcp --dport 22 -j ACCEPT # ssh via tcp

## allow email, all types
iptables -A FORWARD -p tcp --match multiport --dports 143,993,25,587 -j ACCEPT # smtp, imap

### block traffic
## attempt to block autoupdates from oculus, steam,... pretty much impossible. DOES NOT WORK.
#iptables -A OUTPUT -p tcp -d edge-oculus-shv-01-bru2.fbcdn.net --match multiport --dports 80,443 -j REJECT

## block FORWARDED webtraffic.  web via proxy still allowed.  should stop all webbased autoupdates. but also all nonproxied webtraffic.
iptables -I FORWARD -p tcp  --match multiport --dports 80,443 -j REJECT
## block mail
#iptables -A FORWARD -p tcp --match multiport --dports 25,587 -j REJECT

## block all FORWARDED traffic (exceptions should come before this one)  probably the only to block bittorrent.
#iptables -A FORWARD -p tcp -j REJECT
#iptables -A FORWARD -p udp -j REJECT
#iptables -A FORWARD -p icmp -j REJECT #block icmp as well : pings, traceroute,... not recommended


# end of iptables stuff
# watchdog stuff
echo test your config now, and interrupt this script
echo if not, it will reboot this system within 30 seconds
sleep 30
reboot
